from random import randint
import random
class Person:

	def __init__(self, index, m, N,a):
		self.index = index
		self.correspondants = set()
		#simplify so that all senders have same number of correspondants 
		#(alice, number zero, can vary)
		if index == 0:
			m = m + a
		while len(self.correspondants) < m: 
			self.correspondants.add(randint(0, N-1))

class Simulation:

	def __init__(self,m,N,b,n,a):
		self.m = m
		self.n = n
		self.N = N
		self.b = b
		self.persons = [Person(x,m,N,a) for x in range(N)]
		self.done = 0
		self.batches_needed = 0
		self.attacker = Attacker(self, m)

	#simulates sending of messages until attack done
	def simulate_round(self):

		while not self.done: #all senders equally active, constant time for simulation

			senders = set()
			receivers = set()
			while len(senders) < self.b:
				senders.add(randint(0, N-1))
			#make sure all senders actually have at least one correspondant among receivers
			for sender in iter(senders):
				receiver = random.choice([y for y in iter(self.persons[sender].correspondants)])
				receivers.add(receiver)
			while len(receivers) < self.n:
				sender = random.choice([x for x in iter(senders)])#randomize sender
				receiver = random.choice([x for x in iter(self.persons[sender].correspondants)])
				receivers.add(receiver)
	
			#simulated sending and receiving parties from mix and interception
			#actual messages are irrelevant
			self.attacker.intercept(senders, receivers)

		return self.batches_needed

	def stop_simulation(self, batches_needed, thought_correspondants):
		self.done = 1
		self.batches_needed = batches_needed
		if not thought_correspondants.issubset(self.persons[0].correspondants):
			self.batches_needed = -1


class Attacker:

	def __init__(self,simulation, m):
		self.simulation = simulation
		self.batches_needed = 0
		self.batches_with_victim_needed = 0
		self.m = m
		self.receiver_sets = [None for x in range(m)]
		self.gathered_sets = 0

	def learn(self, batch_receivers):

		for x in range(self.gathered_sets):
			s = self.receiver_sets[x]
			if not batch_receivers.isdisjoint(s):
				return
		self.receiver_sets[self.gathered_sets] = batch_receivers.copy()
		self.gathered_sets += 1

	def exclude(self, batch_receivers):
		d = 0
		c = -1
		for i in range(len(self.receiver_sets)):
			s = self.receiver_sets[i]
			if not s.isdisjoint(batch_receivers):
				d += 1
				c = i
		if d == 1:
			self.receiver_sets[c] = self.receiver_sets[c].intersection(batch_receivers)
		
		size = sum(len(s) for s in self.receiver_sets)
		if size == m:
			thought_correspondants = reduce(lambda a, b: a.union(b), self.receiver_sets)
			self.simulation.stop_simulation(self.batches_needed, thought_correspondants)

	def intercept(self,senders,receivers):
		self.batches_needed += 1
		if 0 in senders:
			self.batches_with_victim_needed += 1
			if self.gathered_sets < self.m:
				self.learn(receivers)
			else:
				self.exclude(receivers)
	
def statistics(m,N,b,n,a):
	results = range(4)
	for x in range(4): #average of 4 simulations with same conditions
		print "starting iteration number " + str(x+1) + " of 4"
		sim = Simulation(m,N,b,n,a)
		results[x] = sim.simulate_round()
	average = sum(results)/len(results)
	return average
	
#-------------------------- TEST
m = 4 #number of correspodance partners in system
a = 0 #alices diffrence in number of cerrespondance partners (alice is user zero)
N = 1000 #number of users in system
b = 40 # batch size
n = 10 # receivers in batch
print statistics(m,N,b,n,a) # -1 indicates wrong correspondents found

